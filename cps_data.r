library('gdata')
library('tmap')
library('tmaptools')
library('sf')
library('leaflet')

##################IMPORT PREPARED DATA########################################
#perl <- 'C:/Strawberry/perl/bin/perl.exe'
file = 'excel_files/school_quality/1_sqrp_201718.xls'
df_elem  <- read.xls(file, sheet = 'elementary')
df_hs    <- read.xls(file, sheet = 'hs')
df_combo <- read.xls(file, sheet = 'combo')

df_list <- c(df_elem, df_hs, df_combo)


##################EXPLORATORY ANALYSIS########################################




#SQRP Total Points Earned
df_sqrp <- function(df_input) {
	df <- data.frame('name'    = df_input$School.Name, 
					 'sqrp'    = df_input$SQRP.Total.Points.Earned,
					 'network' = df_input$Network)
	df <- na.omit(df)
	return(df)
}

df_nw_sqrp_means<- function(df_input) {
	df <- df_sqrp(df_input)
	networks <- (unique(df$network))
	
}


mean_sqrp <-function(df_input) {
	df <- df_sqrp(df_input)
	mean(df$sqrp)
}

top_score <- function(df_input) {
	top_score <- df_input[which.max(df_input$sqrp),]
	print(top_score)
}

#find top 5% of schools
df_top10 <- function(df_input) {
	df <- df_sqrp(df_input)
	df <- df[order(-df$sqrp),]
	head(df, n=10)
}

df_bottom10 <- function(df_input) {
	df <- df_sqrp(df_input)
	df <- df[order(df$sqrp),]
	head(df, n=10)

}

##################Plots#######################################################
plot_save <- function(save_title, save_loc) {
	if(save_loc == 1 ) {
		title = title
		number <- length(list.files(path = '.\\boxplots'))
		number <- number + 1
		save_name <- (paste('boxplots/', number, '_', save_title, sep=''))
		return(save_name)
	} else if (save_loc == 2) {
		title = title
		number <- length(list.files(path = '.\\histograms'))
		number <- number + 1
		save_name <- (paste('histograms/', number, '_', save_title, sep=''))
		return(save_name)
	}
}

#jpeg('rplot.jpg', width = 350, height = '350')
df_box <- function(df_input) {
	save_loc = 1
	df <- df_sqrp(df_input) 
	sqrp <- df$sqrp
	title = readline("What is the title of this plot?") 
	save_title <- gsub(" ", "", title, fixed = TRUE)
	save_name <- plot_save(save_title, save_loc)
	
	color <- readline("Would you like to specify a color? Which one?")
	colors <- c('blue', 'red', 'yellow')
	if(!match(color, colors)){
		color = "blue" 
	}	

	pdf(paste(save_name, '.pdf', sep=""))
	boxplot(sqrp, col = color, main = title )
	dev.off()
}

df_hist <- function(df_input) {
	save_loc = 2
	df <- df_sqrp(df_input) 
	sqrp <- df$sqrp
	title = readline("What is the title of this plot?") 
	save_title <- gsub(" ", "", title, fixed = TRUE)
	save_name <- plot_save(save_title, save_loc)

	pdf(paste(save_name, '.pdf', sep=""))
	breaks_input <- strtoi(readline('How many breaks?'))
	hist(sqrp, main = title, breaks = breaks_input, col = 'blue')
	abline(v=median(sqrp), col='magenta', lwd=5)
	dev.off()
	
}

df_networktoInt <- function(df_input) {
	network_list <- list()
	df <- df_sqrp(df_input)
	for (row in 1:nrow(df)) {
		network <- toString((df[row, "network"]))
		network <- strsplit(network, " ")
		network <- as.numeric(network[[1]][2])
		network_list[[row]] <- network
	}
	#rbind(df, do.call(rbind, network_list))
	df$network_nu <- sapply(network_list, paste0, collapse=",")
	df<-df[(df$network_nu %in% c('1', '2','3','4','5','6','7','8','9','10','11','12','13')),]
	df$network_nu <- as.numeric(df$network_nu)

	sqrp_avg = list()
	nums <- 1:13
	for (i in 1:13) {
		net_df <- subset(df, network_nu == i)
		avg_sqrp <- mean(net_df$sqrp)
		sqrp_avg[i] <- avg_sqrp
	}
	df_sqrp_avg <- do.call(rbind, Map(data.frame, network=as.numeric(nums), sqrp_avg=sqrp_avg))

	
}


##################Network Map#################################################
network_shapefile <- "shape_files/geo_export_2c5d365b-de30-445e-bb6d-6361ddd71fb5.shp"
network_map <- read_shape(file=network_shapefile, as.sf = TRUE)
network_map <- network_map[order(network_map$network_nu),]


elem <- df_networktoInt(df_elem)
elem_map <- append_data(network_map, elem, key.shp = 'network_nu', key.data='network')
#elem_map <- qtm(elem_map, 'sqrp_avg')
elem_sqrp_map <- tm_shape(elem_map) +
tm_fill("sqrp_avg", title = 'SQRP', palette = "RdBu" )+
tm_borders(alpha=1) +
tm_text('network_nu', size=0.8)+
tm_legend(title = '2016-17 CPS\nElementary\nSchools\nBy Network', scale= 1.0, legend.title.size = 1, title.position = c("right", "top"), legend.position = c('left', 'bottom'))
tmap_save(tm=elem_sqrp_map, filename='ELementary_SQRP_2016-17')

hs <- df_networktoInt(df_hs)
hs_map <- append_data(network_map, hs, key.shp = 'network_nu', key.data='network')
hs_sqrp_map <- tm_shape(hs_map) +
tm_fill("sqrp_avg", title = 'SQRP', palette = "RdBu" )+
tm_borders(alpha=1) +
tm_text('network_nu', size=0.8)+
tm_legend(title = '2016-17 CPS\nHigh Schools\nBy Network', scale= 1.0, legend.title.size = 1, title.position = c("right", "top"), legend.position = c('left', 'bottom'))
tmap_save(tm=hs_sqrp_map, filename='High_School_SQRP_2016-17')


combo <- df_networktoInt(df_combo)
combo_map <- append_data(network_map, combo, key.shp = 'network_nu', key.data='network')
combo_sqrp_map <- tm_shape(combo_map) +
tm_fill("sqrp_avg", title = 'SQRP', palette = "RdBu" )+
tm_borders(alpha=1) +
tm_text('network_nu', size=0.8)+
tm_legend(title = '2016-17 CPS\nCombo Schools\n By Network', scale= 1.0, legend.title.size = 1, title.position = c("right", "top"), legend.position = c('left', 'bottom'))
tmap_save(tm=combo_sqrp_map, filename='Combo_School_SQRP_2016-17')

